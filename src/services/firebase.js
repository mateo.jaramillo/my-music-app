import { getAuth, onAuthStateChanged, createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut, updateProfile } from "firebase/auth";
// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";


// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDgIkz6WelkARVQ8ttfkCf29YXd0iQdVgg",
    authDomain: "my-music-app-2d264.firebaseapp.com",
    projectId: "my-music-app-2d264",
    storageBucket: "my-music-app-2d264.appspot.com",
    messagingSenderId: "187547489858",
    appId: "1:187547489858:web:e7d9fb962690ac1cd13fdb"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const auth = getAuth();

// export const logoutUser = () => {

//     signOut(auth).then(() => {
//         window.location.href = "/";
//     }).catch((error) => {
//         alert("No hay una sesión iniciada");
//     });
// }
