import store from "../store";

const client_id = '910bb4e45cd44aacaca07eea2bc5e61a';
const client_secret = '00095c77787448a9b9cce19b11aa712a';
const API_URL = 'https://accounts.spotify.com/api/token';
const musicUrl = 'https://api.spotify.com/v1/browse/new-releases';

const getToken = () => {
    const authOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic ' + btoa(client_id + ':' + client_secret)
      },
      body: 'grant_type=client_credentials'
    };

    fetch(API_URL, authOptions)
      .then(result => result.json())
      .then(token => {
        localStorage.setItem("token", token.access_token);
        getData();
      })
  }

  const getData = () => {

    const getOptions = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem("token")}`
      }
    };

    const songs = [];
    fetch(musicUrl, getOptions)
      .then(result => result.json())
      .then(data => {
        // console.log(data.albums.items.);
        data.albums.items.map((song) => {
            const newSong = {id: song.id, artists: song.artists, images: song.images, name: song.name, favorite: false};
            songs.push(newSong);
        })

        store.dispatch({
          type: "@songs/add",
          payload: songs
        })
      });
  };

export default getToken;
