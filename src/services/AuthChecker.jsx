import React from "react";
import { useSelector } from "react-redux";
import { useUserContext } from "../context/user";
import { Redirect } from "react-router-dom";

const AuthChecker = ({ children }) => {
  
  const user = useSelector((state) => state.user)
  // const userState = useSelector((state) => state.user);

  if (user === null) {
    return <Redirect to="/login" />;
  }
  return children;
};

export default AuthChecker;
