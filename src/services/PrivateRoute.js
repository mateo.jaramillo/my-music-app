import React from "react";
import { Redirect, Route } from "react-router-dom";
import { auth } from "./firebase";
import { useSelector } from "react-redux";

// const PrivateRoute = (props) => {
const PrivateRoute = ({ component: Component, ...rest }) => {
  // console.log("From private route", user);
  // const user = auth.currentUser;
  const user = useSelector((state) => state.user);
  if (user === null) {
    return <Redirect to="/login" />;
  } else {
    return <Route {...rest} component={Component} />;
  }
};

export default PrivateRoute;
