import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import Favorites from "./components/pages/Favorites";
import Home from "./components/pages/Home";
import Inicio from "./components/pages/Inicio";
import Principal from "./components/templates/Principal";
import Login from "./components/pages/Login";
import { getAuth } from "firebase/auth";
import { useState } from "react";
import { useEffect } from "react";
import { onAuthStateChanged } from "firebase/auth";
// import { auth } from './services/firebase';
import { app, auth } from "./services/firebase";
import AuthChecker from "./services/AuthChecker";
import { useSelector } from "react-redux";
import { UserContext } from "./context/user";
import NotFound from "./components/pages/NotFound";
import PrivateRoute from "./services/PrivateRoute";
import { ToastProvider } from "react-toast-notifications";

function App() {
  // const [user1, setUser] = useState(auth.currentUser);
  // const user = useSelector((state) => state.user);
  // useEffect( () => {
  //   setUser(user);
  // } ,[user])

  // console.log(user);
  return (
    // <UserContext.Provider value={{ user, setUser }}>
    <ToastProvider>
      <Router>
        <Switch>
          <Route exact path="/login" component={Login} />

          <Route path={["/home", "/favorites"]}>
            <Principal>
              <Switch>
                <PrivateRoute exact path="/home" component={Home} />
                <PrivateRoute exact path="/favorites" component={Favorites} />
              </Switch>
            </Principal>
          </Route>
          <Route exact path="/" component={Inicio} />
          <Route path="*" component={NotFound} />
        </Switch>
      </Router>
    </ToastProvider>
    // </UserContext.Provider>
  );
}

export default App;
