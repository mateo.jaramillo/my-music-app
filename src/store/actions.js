const ADD_SONGS = {
    type: "@songs/add"
}

const ADD_FAVORITE = {
    type: "@favorites/add"
}

const REMOVE_FAVORITE = {
    type: "@favorites/remove"
}

const SET_USER = {
    type: "@user/set"
}
const RESET = {
    type: "@reset"
}

const ACTIONS = {
    ADD_SONGS,
    ADD_FAVORITE,
    REMOVE_FAVORITE,
    SET_USER,
    RESET
}

export default ACTIONS