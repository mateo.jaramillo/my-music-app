// El reducer es Switch case con cada accion 
import ACTIONS from "./actions";

const spotifyReducer = (state, action) =>{
    switch (action.type) {
        case ACTIONS.ADD_SONGS.type:
            return {...state, "songs": action.payload}

        case ACTIONS.ADD_FAVORITE.type:
            return {...state, favorites: [...state.favorites, action.payload]}
            
        case ACTIONS.REMOVE_FAVORITE.type:
            const newData = state.favorites.filter((item) => {
                return item !== action.payload
            })
            return {...state, favorites: newData}

        case ACTIONS.SET_USER.type:
            return  {...state, user: action.payload}

        case ACTIONS.RESET.type:
            return action.payload

        default:
            return state;
    }

}

export default spotifyReducer