// Donde se crea la store
import { createStore } from "redux";
import spotifyReducer from "./reducer";

const preloadedState = {songs: [], favorites: [], user: null};

// Esta tiene como parámetros el reducer
const store = createStore(
    spotifyReducer,
    preloadedState,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

export default store
