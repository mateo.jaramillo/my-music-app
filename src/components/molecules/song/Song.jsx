import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import "./Song.scss"

const Song = ({ track }) => {

    const dispatch = useDispatch()
    useSelector(state => state.favorites)

    // let artistas = "";
    const artistas = track.artists.map((artista, indice) => {
        if (indice > 0) {
            return ", " + artista.name
        } else {
            return artista.name
        }
    });

    return (
        <div className='song'>
            <figure className='song__figure'>
                <img className='song__image' src={track.images[0].url} alt="" />
            </figure>
            <div className="card__text">
                <div className='song__info'>
                    <div>
                        <h2 className='song__name'>{track.name}</h2>
                        <p className='artist__name'>{artistas}</p>
                    </div>
                </div>
                <i onClick={() => {
                    track.favorite = !track.favorite;
                    if (track.favorite) {
                        dispatch({
                            type: "@favorites/add",
                            payload: track
                        })
                    } else {
                        dispatch({
                            type: "@favorites/remove",
                            payload: track,
                        })
                    }

                }} className={`fa-solid fa-heart song__icon ${track.favorite ? "like" : " "}`} />
            </div>
        </div>
    )
}

export default Song