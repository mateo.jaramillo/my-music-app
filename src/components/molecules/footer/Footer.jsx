import React from "react";
import "./footer.scss";

const Footer = () => {
  return (
    <footer className="footer">
      
      <section className="footer__app">
        <h2>My Music App</h2>
        <br />
        <p>
          Este es un ejercicio práctico para desarrollador front end utilizando
          diferentes teconologías de desarrollo web.
        </p>
        <br />
        <p>
          Se basa en el desarrollo de una web responsive que consuma la API de
          Spotify, para traer una lista de canciones. Un usuario, luego de
          registrarse y loguearse, podrá ver la lista de canciones y podrá
          agregar las que más le gusten a sus favoritos.
        </p>
      </section>
      <section className="footer__links">
        <ul>
          <li className="footer__link">Contáctate con nosotros</li>
          <li className="footer__link">Sobre nosotros</li>
          <li className="footer__link">Dónde encontrarnos</li>
        </ul>
      </section>
    </footer>
  );
};

export default Footer;
