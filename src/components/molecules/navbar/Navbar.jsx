import React from "react";
import { NavLink, Redirect } from "react-router-dom";
import "./Navbar.scss";
import user_icon from "../../../assets/img/user_icon.jpg";
import { app, auth } from "../../../services/firebase";
import { signOut } from "firebase/auth";
import { useDispatch, useSelector } from "react-redux";

const Navbar = () => {

  const username = useSelector((state) => {
    try {
      return state.user.user.displayName.toUpperCase();
    } catch (error) {
      return "";
    }
  });
  // let username = "";
  // if (auth) {
  //   username = auth.currentUser.displayName.toUpperCase();
  // }
  // const username = auth.currentUser.displayName.toUpperCase();
  const dispatch = useDispatch();
  const logoutUser = () => {
    // console.log("Cerrando sesion");
    signOut(auth).then(() => {
      // console.log("Login Out");
      dispatch({
        type: "@reset",
        payload: { songs: [], favorites: [], user: null },
      });
      return <Redirect to="/" />;
      // props.setUser(null);
      // localStorage.setItem("user", null);
      // window.location.href = "/login"
    });
  };
  // console.log(auth);
  return (
    <nav className="navbar">
      <div className="navbar__links">
        <NavLink to="/home" className="navbar__link">
          <i className="fa-solid fa-home navbar__icon" />
          <p>Home</p>
        </NavLink>

        <NavLink to="/favorites" className="navbar__link">
          <i className="fa-solid fa-heart navbar__icon" />
          <p>Favoritos</p>
        </NavLink>
      </div>
      <div className="navbar__user">
        <figure className="navbar__figure">
          <img src={user_icon} alt="User icon" className="navbar__image" />
        </figure>
        <p className="user__name">{username}</p>
        <button onClick={() => logoutUser()} className="navbar__button">
          Cerrar Sesión
        </button>
      </div>
    </nav>
  );
};

export default Navbar;
