import React from "react";
import Navbar from "../../molecules/navbar/Navbar";
import { Link, Redirect } from "react-router-dom";
import "./Header.scss";
import spotify_logo from "../../../assets/img/spotify_logo.jpg";
import { useState } from "react";
import { auth } from "../../../services/firebase";
import { useDispatch, useSelector } from "react-redux";
import { signOut } from "firebase/auth";
// const user = "Mateo";

const Header = () => {
    const user = useSelector((state) => {
        try {
            // console.log("state.user: ",state.user.user.displayName);
            return state.user.user.displayName.toUpperCase();
        } catch (error) {
            return "";
        }
    })
    
  const dispatch = useDispatch();
  const logoutUser = () => {
    // console.log("Cerrando sesion");
    signOut(auth).then(() => {
      // console.log("Login Out");
      dispatch({
        type: "@reset",
        payload: { songs: [], favorites: [], user: null },
      });
      return <Redirect to="/home" />;
      // props.setUser(null);
      // localStorage.setItem("user", null);
      // window.location.href = "/login"
    });
  };

  const [mostrarNavegacion, setMostratNavegacion] = useState(false);

  return (
    <>
      <header className="header">
        <Link to="/home">
          <figure className="header__figure">
            <img
              src={spotify_logo}
              alt="Spotify icon"
              className="header__image"
            />
          </figure>
        </Link>
        <Navbar className="header__navbar" />
        <i
          className={`fa-solid ${mostrarNavegacion ? "fa-times" : "fa-bars"}`}
          onClick={() => setMostratNavegacion(!mostrarNavegacion)}
        />
      </header>
      {mostrarNavegacion && (
        <ul className="responsive__routes">
          <ResponsiveRoute nombre="Home" ruta="/home" />
          <ResponsiveRoute nombre="Favoritos" ruta="/favorites" />
          {/* <ResponsiveRoute nombre="Cerrar Sesión" ruta="#"  /> */}
          <Link to={""} className="responsive__link">
            <li onClick={() => logoutUser()}>Cerrar Sesión</li>
          </Link>
          <li className="userName">{user}</li>
        </ul>
      )}
    </>
  );
};

const ResponsiveRoute = ({ nombre, ruta }) => {
  return (
    <Link to={ruta} className="responsive__link">
      <li>{nombre}</li>
    </Link>
  );
};

export default Header;
