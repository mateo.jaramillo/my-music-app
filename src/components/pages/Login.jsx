import React from "react";
import { useState } from "react";
import "./Login.scss";
import spotify_logo from "../../assets/img/spotify_logo.jpg";
import { app, auth } from "../../services/firebase";
import {
  createUserWithEmailAndPassword,
  onAuthStateChanged,
  signInWithEmailAndPassword,
  updateProfile,
} from "firebase/auth";
import { useDispatch, useSelector } from "react-redux";
import { useUserContext } from "../../context/user";
import { Redirect } from "react-router-dom";
import { useToasts } from 'react-toast-notifications';





const Login = () => {
  const { addToast } = useToasts();
  // const {setUser} = useUserContext();
  // const { Navigate } = require("react-router-dom");
  const dispatch = useDispatch();
  const [loginForm, setLoginForm] = useState(true);
  // const [signed, setSigned] = useState(false)
  // const [email, setEmail] = useState();
  const submitHandler = (e) => {
    // console.log("handling event");
    e.preventDefault();
    const name = e.target.name.value;
    const email = e.target.email.value;
    const password = e.target.password.value;

    if (loginForm) {
      loginUser(email, password);
    } else {
      registerUser(email, password, name);
    }
  };

  const loginUser = (email, password) => {
    // console.log("Auth before singin: ", auth);
    signInWithEmailAndPassword(auth, email, password).then((user) => {
      // console.log("Auth AFTER singin: ", auth);
      // console.log("Signed correctly, user: ", user);
      // props.setUser(user);
      dispatch({
        type: "@user/set",
        payload: user,
      });
      addToast('Bienvenido', { appearance: 'success', autoDismiss: true, autoDismissTimeout: 2500 });
      // setUser(user);
      // localStorage.setItem("user", user);
      // window.location.href = "/home"
    }).catch((error) => {
      // alert("Datos erróneos: ", error.message);
      addToast(error.message, { appearance: 'error', autoDismiss: true, autoDismissTimeout: 2500 });
      // console.log(error.message);
    });
  };

  const registerUser = (email, password, name) => {
    createUserWithEmailAndPassword(auth, email, password).then((user) => {
      // props.setUser(user);
      updateProfile(auth.currentUser, {
        displayName: name,
      }).then(() => {
        // console.log("Profile updated");
        dispatch({
          type: "@user/set",
          payload: user,
        });
        addToast('Usuario creado, ¡Bienvenido!', { appearance: 'success', autoDismiss: true, autoDismissTimeout: 2500 });
      })
      // localStorage.setItem("user", user);

      // app
      //   .auth()
      //   .updateProfile(app.auth().currentUser, {
      //     displayName: name,
      //   })
      //   .then(() => {
      //     console.log("Display Name Updated", name);
      //   });
    }).catch((error) => {
      addToast(error.message, { appearance: 'error', autoDismiss: true, autoDismissTimeout: 2500 });
    });
  };

  const userState = useSelector((state) => state.user)
  // console.log(userState);

  if (userState) {
      return <Redirect to="/home" />
  } else {
    return (
      <section className="login__container">
        <figure className="logo__figure">
          <img src={spotify_logo} alt="Spotify Logo" className="logo__image" />
        </figure>
        <div className="login">
          <h1 className="login__title">{loginForm ? "Iniciar Sesión" : "Registrarse"}</h1>
          <br />
          <form className="login__form" onSubmit={submitHandler}>
            {loginForm ? <LoginForm /> : <RegisterForm />}
          </form>
          <br />
          <p>
            {loginForm ? "No tienes una cuenta?" : "Ya tienes una cuenta?"}{" "}
            <a href="#" onClick={() => setLoginForm(!loginForm)}>
              {loginForm ? "Regístrate" : "Iniciar Sesión"}
            </a>
          </p>
        </div>
      </section>
    );
  }

};

const LoginForm = () => {
  // const [name, setName] = useState();
  // const [email, setEmail] = useState();
  // const [password, setPassword] = useState();

  return (
    <>
      <label htmlFor="email" className="login__label">Correo Electrónico</label>
      <input
        className="login__input"
        type="email"
        name="email"
        id="email"
        placeholder="Ingrese su correo electrónico"
        required
      />

      <label htmlFor="password" className="login__label">Contraseña</label>
      <input
        className="login__input"
        type="password"
        name="password"
        id="password"
        placeholder="Ingrese su contraseña"
        required
      />
      {/* <label htmlFor="email">Correo Electrónico</label>
      <input className='login__input' type="email" name="email" id='email' placeholder='Ingrese su correo electrónico' onChange={(event) => {
        setEmail(event.target.value);
      }} required />

      <label htmlFor="password">Contraseña</label>
      <input className='login__input' type="password" name="password" id='password' placeholder='Ingrese su contraseña' onChange={(event => {
        setPassword(event.target.value);
      })} required /> */}

      <button className="login__button" type="submit">
        Iniciar Sesión
      </button>
    </>
  );
};

const RegisterForm = () => {
  // const [name, setName] = useState();
  // const [email, setEmail] = useState();
  // const [password, setPassword] = useState();

  return (
    <>
      <label htmlFor="name" className="login__label">Nombre completo</label>
      <input
        type="text"
        name="name"
        className="login__input"
        id="name"
        placeholder="Nombre completo"
        required
      />

      <label htmlFor="email" className="login__label">Correo Electrónico</label>
      <input
        className="login__input"
        type="email"
        name="email"
        id="email"
        placeholder="Ingrese su correo electrónico"
        required
      />

      <label htmlFor="password" className="login__label">Contraseña</label>
      <input
        className="login__input"
        type="password"
        name="password"
        id="password"
        placeholder="Ingrese su contraseña"
        required
      />

      <button className="login__button" type="submit">
        Registrarse
      </button>
    </>
  );
};

export default Login;
