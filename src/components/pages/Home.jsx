import React, { useEffect } from "react";
import { useState } from "react";

import { useSelector } from "react-redux";
import { Navigate } from "react-router-dom";

import getToken from "../../services/spotify";
import store from "../../store/index";
import Song from "../molecules/song/Song";

const Home = () => {
  
  const music = useSelector((state) => state.songs);
  // const [user, setUser] = useState(null)

  useEffect(() => {
    if (store.getState().songs.length === 0) {
      getToken();
    }
  }, []);
  
  return (
    <HomeContent music={music} />
  )
};


const HomeContent = ({ music }) => {
  return (
    <div className="page">
      <h1 className="page__title">Agregadas Recientemente</h1>
      <div className="page__songs">
        {music.map((cancion) => (
          <Song track={cancion} key={cancion.id} />
        ))}
      </div>
    </div>
  );
};

export default Home;
