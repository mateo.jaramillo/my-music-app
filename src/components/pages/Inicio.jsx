import React from "react";
import "./Inicio.scss";
import spotify_logo from "../../assets/img/spotify_logo.jpg";
import { useHistory, NavLink } from "react-router-dom";

const Inicio = () => {
  // const history = useHistory();
  return (
    <main className="inicio">
      <div className="inicio__header">
        <h1 className="inicio__title">MY MUSIC APP</h1>
        <span className="inicio__logo logo">
          <p>Powered by</p>
          <figure className="logo__figure">
            <img
              src={spotify_logo}
              alt="Spotify Logo"
              className="logo__image"
            />
          </figure>
        </span>
      </div>

      <NavLink exact to="/login">
        <button className="button">INICIAR SESION</button>
      </NavLink>
    </main>
  );
};

export default Inicio;
