import React from 'react'
import Song from '../molecules/song/Song';
import { useSelector } from 'react-redux';

const Favorites = () => {
  
  const favoriteSongs = useSelector(state => state.favorites)

  return (
    <div className='page'>
      <h1 className='page__title'>Favoritos</h1>
      <div className="page__songs">
        {favoriteSongs.length === 0 && <h2 className='page__title'>No Music Found</h2>}
        {favoriteSongs.map((song) =>
          <Song track={song} key={song.id}/>
        )}
      </div>
    </div>
  )
};

export default Favorites;