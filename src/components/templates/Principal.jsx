import React from 'react'
import Footer from '../molecules/footer/Footer';
import Header from '../organisms/header/Header';

const Principal = ({children}) => {

    return (
        <div className='principal'>
            <Header/>
            <main className='main'>
                {children}
            </main>
            <Footer />
        </div>
    )
};

export default Principal;